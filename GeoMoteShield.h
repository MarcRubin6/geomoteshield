#include "Arduino.h"
#include "SPI.h"
#include "SPISRAM.h"
#include "ADC_7714.h"

#define ADC_PIN 2
#define SRAM_PIN 9
#define SD_PIN 8
#define SRAM_SIZE 32768

class GeoMoteShield
{
    public:
        GeoMoteShield();
        void start_sampling();
        void stop_sampling();
        void setHighPrecision(byte p);
        void transmit_data();
        void transmit_data_ascii();
    
        ADC_7714 *adc;
    
    private:
        byte high_precision;
        void transmit16();
        void transmit24();
    
        void transmit16_ascii();
        void transmit24_ascii();
};

void sense16();
void sense24();

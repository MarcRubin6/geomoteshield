#include <GeoMoteShield.h>

ADC_7714 _adc(ADC_PIN);
SPISRAM sram(SRAM_PIN);

unsigned int ptr;
unsigned long count;

byte SPI_LOCK;

GeoMoteShield::GeoMoteShield()
{
    _adc.setPin(ADC_PIN);
    sram.setPin(SRAM_PIN);

    adc = (ADC_7714 *) &_adc;
    
    //set SD pin to HIGH to ignore
    pinMode(SD_PIN, OUTPUT);
    digitalWrite(SD_PIN,HIGH);
    
    //setup default SPI for SRAM
    pinMode(SRAM_PIN, OUTPUT);
    pinMode(SRAM_PIN, HIGH);
    
    //setup ADC
    adc->initialize();
    adc->setChannel(CHANNEL_1_2);
    adc->setGain(GAIN_1);
    adc->setPolarity(BIPOLAR);
    high_precision = false;
    adc->setWordLength((high_precision) ? WORD_LENGTH_24 : WORD_LENGTH_16);
    adc->setFrequency(F500); //F200
    
    //setup default SPI for SRAM
    pinMode(SRAM_PIN, OUTPUT);
    SPI.begin();
    SPI.setBitOrder(MSBFIRST);
    SPI.setClockDivider(SPI_CLOCK_DIV2);
    SPI.setDataMode(SPI_MODE0);
    
    SPI_LOCK = true;
    adc->calibrate();
    adc->stop();
    SPI_LOCK = false;
    
    ptr = 0;
}

void GeoMoteShield::start_sampling()
{
    if(high_precision){
        attachInterrupt(1, sense24, FALLING);
    } else {
        attachInterrupt(1, sense16, FALLING);
    }
    ptr = 0;
    adc->start();    
}

void GeoMoteShield::stop_sampling()
{

    adc->stop();
    detachInterrupt(1);
}

void GeoMoteShield::setHighPrecision(byte p)
{
    high_precision = p;
    adc->setWordLength((high_precision) ? WORD_LENGTH_24 : WORD_LENGTH_16);
}

void GeoMoteShield::transmit_data()
{
    
    if(high_precision){
        transmit24();
    } else {
        transmit16();
    }
}


void GeoMoteShield::transmit_data_ascii()
{
    if(high_precision){
        transmit24_ascii();
    } else {
        transmit16_ascii();
    }
}

void sense16()
{
    
    if(SPI_LOCK || ptr >= SRAM_SIZE){
        return;
    }
    
    SPI_LOCK = true;
    unsigned int val = _adc.getVoltageInt();
    sram.write(ptr, (byte *) &val, sizeof(val));
    SPI_LOCK = false;
    ptr += sizeof(val);
    
}

void sense24()
{
    if(SPI_LOCK || ptr >= SRAM_SIZE){
        return;
    }
    
    SPI_LOCK = true;
    unsigned long val = _adc.getVoltageLong();
    sram.write(ptr, (byte *) &val, sizeof(val));
    SPI_LOCK = false;
    ptr += sizeof(val);
}


void GeoMoteShield::transmit16()
{
    byte b[2];
    for(unsigned int i = 0; i < ptr; i += 2){
        sram.read(i, b, 2);
        Serial.write(b,2);
    }
}

void GeoMoteShield::transmit24()
{
    byte b[4];
    for(unsigned int i = 0; i < ptr; i += 4){
        sram.read(i, b, 4);
        Serial.write(b,4);
    }
}

void GeoMoteShield::transmit16_ascii()
{
    byte b[2];
    for(unsigned int i = 0; i < ptr; i += 2){
        sram.read(i, b, 2);
        
        unsigned int *v = (unsigned int *) b;
        Serial.println(*v);
    }
}

void GeoMoteShield::transmit24_ascii()
{
    byte b[4];
    for(unsigned int i = 0; i < ptr; i += 4){
        sram.read(i, b, 4);
        
        unsigned long *v = (unsigned long *) b;
        Serial.println(*v);
    }
}
